# #!/usr/bin/env python3
# from flask import Flask #server
# from flask import request #to handle the different http requests
# from flask import Response #to reply (we could use jsonify as well but we handled it)
# import json
# #My libraries
# from message_catalog import MessageCatalog
# from message import Message

# app = Flask(__name__)

# #“Routes” will handle all requests to a specific resource indicated in the
# #@app.route() decorator

# url="http://localhost:5000"

# #These route are for the "person" use case #serveur
# 	#case 1 refuse GET
# @app.route('/', methods=['GET'])
# def index():
#     Response(msg)
#     r = requests.get(url) #je demande la liste THEY SAID à l'hopital

# 	pass ## TODO:

# 	#case 2 hear message
# @app.route('/<msg>', methods=['POST'])
# def add_heard(msg):
#     r = request.get_json() #je déclare le covid et j'informe l'hopital

# 	pass ## TODO:
# #End of person use case

# #Hospital use case  #client
# @app.route('/they-said', methods=['GET','POST'])
# def hospital():
#     if r.method=='GET': #on me demande la liste des they said
#         Response(msg) #j'envoie la liste des they said
#     else if r.method=='POST': #on m'informe qu'une personne a le covid
#         r = requests.get(url) #je reçois la liste des messages I said
#         Response(msg2) #je confirme la bonne réception

# 	pass ## TODO: 
# #End hospital use case
    
# #will only execute if this file is run
# if __name__ == "__main__":
# 	catalog = MessageCatalog()
# 	app.run(host="0.0.0.0", debug=False)


from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__) #création d'un objet Flask appelé app

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET']) #ouvrir une connection GET et appliquer la méthode index
def index():
	response = render_template("menu.html",root=request.url_root, h="")#On réalise des tests UI et on stock la réponse dans response
	return response 

	#case 2 hear message
@app.route('/<msg>', methods=['POST']) #ouvrir une connection POST et appliquer la méthode add_heard
def add_heard(msg):
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)): #si on a correctement ajouté le message dans la colonne IHEARD
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #on crée la réponse du serveur qui indique que le message a bien été ajouté au catalogue (201 Created)
	else : #si on n'a pas correctement ajouté le message
		reponse = Response(status=400) #on crée la réponse du serveur qui indique que le serveur n'a pas compris la requête (400 Bad Request)
	return response
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST']) #ouvrir une connection multiple et appliquer la méthode hospital
def hospital():
	if request.method == 'GET': #si on se connecte à la méthode GET
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY) #on purge les messages THEY trop anciens (age>14)
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #on crée la réponse du serveur qui indique qu'on a bien exporté les messages THEY (200 OK)
	elif request.method == 'POST': #si on se connecte à la méthode POST
		if request.is_json: #si les données envoyées par le client sont en json
			req = json.loads(request.get_json()) #on convertit la requête en python
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY) #on importe les messages THEY
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201) #on crée la réponse du serveur qui indique qu'on a bien importé les messages THEY (201 Created)
		else: #si les données ne sont pas en json
			response = Response(f"JSON expected", mimetype='text/plain', status=400) #on crée la réponse du serveur qui indique que le serveur n'a pas compris la requête (400 Bad Request) : il faut des données au format json
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403) #si on n'utilise ni la méthode GET ni POST, on crée la réponse du serveur qui indique que la requête n'est pas autorisée (403 Forbidden)
	return response
#End hospital use case

#UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET']) #ouvrir une connection GET et appliquer la méthode check
def check(host):
	h = host.strip() #enlever les potentiels espaces de l'adresse souhaitée (pour ne pas avoir d'erreur)
	n = client.get_covid(h)#on ajoute au catalogue tous les cas covid du serveur h
	r = dict()#équivalent en python d'un objet Json
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"#on met dans r["text"] les messages THEYSAID importé du serveur h
	if client.catalog.quarantine(4):#Si on a croisé plus de 4 personnes ayant le covid
		r["summary"] = "Stay home, you are sick."#on considère que la personne est malade
	else:
		r["summary"] = "Everything is fine, but stay home anyway."#si la personne a croisé 4 personnes ou moins, elle n'est pas malade mais doit se confiner quand même
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)#on retourne le résultat de ce test UI

@app.route('/declare/<host>', methods=['GET']) #ouvrir une connection GET et appliquer la méthode declare
def declare(host):
	h = host.strip()#enlever les potentiels espaces de l'adresse souhaitée (pour ne pas avoir d'erreur)
	client.send_history(h)#on envoie la liste des messages ISAID au serveur h
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",#on déclare au serveur h qu'on a le covid
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)#on retourne le résultat de ce test UI

@app.route('/say/<hosts>', methods=['GET']) #ouvrir une connection GET et appliquer la méthode tell
def tell(hosts):
	hosts = hosts.split(",")#on sépare les serveurs par une virgule
	r=[]#on crée une liste
	for host in hosts:#on parcourt tous les serveurs
		h = host.strip()#enlever les potentiels espaces de l'adresse souhaitée (pour ne pas avoir d'erreur)
		client.say_something(h)#envoie un message ISAID au serveur h
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",#on ajoute à la liste le message ISAID envoyé au serveur h (on réitère l'opération pour tous les serveurs)
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)#on retourne le résultat de ce test UI
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
