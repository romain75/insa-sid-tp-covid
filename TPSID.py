# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 17:47:06 2020

@author: blanc
"""

#!/usr/bin/env python3
import json
import time
import datetime
#replaced by the following two lines for testing purposes
# import falsifytime
# time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256
import string
import secrets

# class Message : #fait en séance 1
    
#     def __init__(self,d):
#         self.date=d
#         self.texte=self.genererMessage()
        
#     def genererMessage(self):
        

class Message:
    #the constructor can take various parameters
    #default is parameterless, it generates a message to be said
    #if type is not specified, it is to be imported from an export string or parsed export string
    #otherwise, if date is False the time is system time
        #else it is a float, then the date should be this parameter
    def __init__(self, msg="", msg_type=False, msg_date=False):
        self.msg_date=datetime.datetime.today()
        self.msg=self.generate()
        self.msg_type=False #msg_type=False : message reçu / msg_type=True : message envoyé
        
        
    #a method to compute the age of a message in days or in seconds
    def age(self, days=True):
        
        #if self.days=True:datetime.datetime.today().day
        age = self.msg_date - datetime.datetime.today()
        return age
    
    #a class method that generates a random message
    @classmethod
    def generate(self):
        
        alphabet = string.ascii_letters + string.digits
        password = ''.join(secrets.choice(alphabet) for i in range(8))
        chaine = self.msg_date + password
        return(chaine)
    

    #a method to convert the object to string data
    def __str__(self):
        return "Le message est :"+self.msg+"de type "+self.msg_type+"date du "+self.msg_date
    
    #export/import
    def m_export(self):
        #write in a xml/json file
        with write(msg,"w") as input_xml:
            messages = minidom.write(input_xml)
        return messages

    def m_import(self, msg):
        #import a xml/json file
        with open(msg,"r") as input_xml:
            messages = minidom.parse(input_xml)
        return messages

#will only execute if this file is run
# if __name__ == "__main__":
#     #test the class
#     myMessage = Message()
#     time.sleep(1)
#     mySecondMessage = Message(Message.generate(), Message.MSG_THEY)
#     copyOfM = Message(myMessage.m_export())
#     print(myMessage)
#     print(mySecondMessage)
#     print(copyOfM)
#     time.sleep(0.5)
#     print(copyOfM.age(True,True))
#     time.sleep(0.5)
#     print(copyOfM.age(False,True))


class MessageCatalog:
    #constructor, loads the messages (if there are any)
    def __init__(self, filepath):
        with open(filepath,"w") as input_json:
            messagelist = json.load(input_json)
        return messagelist

    # #destructor (optional, if you opened a file, close it)
    # def __del__(self):

    #Import object content (can import batch of THEY SAID or other for testing purposes)
    def c_import(self, content):
        if content.msg_type=False:
            

    #export the messages of a given type from a catalog
    def c_export_type(self, msg_type):
        if msg_type=False:
            

    # #Export the whole catalog under a text format (optional)
    # def c_export(self):

    #add a Message object to the catalog
    def add_message(self, m, save=True):

    #remove all messages of msg_type that are older than max_age days
    #msg_type is false for deleting all messages of all types older than max_age
    def purge(self, max_age=14, msg_type=False):

    #Check if message string is in a category
    def check_msg(self, msg_str, msg_type=Message.MSG_THEY):

    #Say if I should quarantine based on the state of my catalog
    def quarantine(self, nb_heard=3):
        count=0
        for i in MessageCatalog(): #je parcours ma liste de messages
            if i.msg_type=THEY : #si c'est un message du type THEY_SAID (ie il vient de l'hopital)
                count=count+1 #j'incrémente un compteur
        
        if count>=nb_heard:
            print("Vous devez vous confiner !")

        
        
#will only execute if this file is run
# if __name__ == "__main__":
#     #test the class
#     catalog = MessageCatalog("test.json")
#     catalog.add_message(Message())
#     catalog.add_message(Message(Message.generate(), Message.MSG_THEY))
#     print(catalog.c_export())
#     time.sleep(0.5)
#     catalog.add_message(Message(Message.generate(),Message.MSG_IHEARD))
#     catalog.add_message(Message())
#     print(catalog.c_export())
#     time.sleep(0.5)
#     catalog.add_message(Message())
#     print(catalog.c_export())
#     time.sleep(2)
#     catalog.purge(2)
#     print(catalog.c_export())



m=Message("21-03", False, False)
print(m)