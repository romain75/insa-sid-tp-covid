#!/usr/bin/env python3
import json, os
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath):
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ] #le catalogue est une matrice avec 3 colonnes (une colonne par type de message)
		self.filepath = filepath #indique le chemin du fichier où sera édité le catalogue
		if os.path.exists(self.filepath): #si le chemin existe déjà (ie le fichier est déjà créé)
			self.open() #on ouvre le fichier déjà créé
		else: #si le fichier n'existe pas encore
			self.save() #on enregistre ce nouveau fichier

	#destructor closes file.
	def __del__(self):
		del self.filepath #fermer le fichier si un est déjà ouvert
		del self.data #fermer les données

	#get catalog size
	def get_size(self, msg_type=False):
		if(msg_type is False): 
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY) #donne le nombre total de messages dans le catalogue en additionnant les nombres d'éléments de chaque colonne (calculés dans le else)
		else: #méthode récursive
			res = len(self.data[msg_type]) #compte le nombre d'éléments dans la colonne associée à msg_type
		return res

	#Import object content
	def c_import(self, content, save=True):
		i = 0 #on initialise le compteur à 0
		for msg in content:
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False): #si on a ajouté un message au catalogue
					i=i+1 #on incrément le compteur de message
		if save: #si on ne donne pas de paramètre
			self.save() #on sauvegarde le fichier par défaut
		return i #retourne le nombre de message importés

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY):
		i=0 #on initialise le compteur à 0
		for msg in content:
			m = Message(msg) #on crée un nouveau message qui s'appelle m
			m.set_type(new_type) #on change son type en MSG_THEY
			if self.add_message(m, False):#si on a bien ajouté le message dans le catalogue
				i=i+1 #on incrément le compteur
		if i > 0: #si on ajouté au moins un élément dans le catalogue
			self.save() #on sauvegarde le fichier
		return i #on retourne le nombre d'éléments ajoutés dans le catalogue

	#Open = load from file
	def open(self):
		file = open(self.filepath,"r") #ouvrir le fichier en mode "read"
		self.c_import(json.load(file), False) #on importe le catalogue dans le fichier
		file.close() #on ferme le fichier

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True):
		tmp = int(bracket) * "[\n" #la première ligne de tmp est un crochet avec un retour à la ligne
		first_item = True
		for msg in self.data[msg_type]: #on parcourt une colonne du catalogue (déterminée par msg_type)
			if first_item: #si c'est le premier élément, on ne saute pas de ligne (pas de passage dans le else) et on rajoute le message (ligne 69)
				first_item=False #indiquer qu'on a déjà rajouté le premier élément
			else: #si ce n'est pas le premier élément de la colonne
				tmp = tmp +",\n" #on va à la ligne
			tmp = tmp + (indent*" ") +  msg.m_export() #rajouter l'indentation définie en paramètre et le contenu du message au format standardisé par la méthode m_export()
		tmp = tmp + int(bracket) * "\n]" #la dernière ligne de tmp est un crochet
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2):
		tmp = ""
		if(len(self.data[Message.MSG_ISAID])> 0 ): #s'il y a des messages dans la colonne MSG_ISAID
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False) #on rajoute le contenu de la colonne MSG_ISAID (écrit au bon format grâce à la méthode c_export_type)
		if(len(self.data[Message.MSG_IHEARD]) > 0): #s'il y a des messages dans la colonne MSG_IHEARD
			if tmp != "": #si on a déjà écrit le contenu d'une colonne dans tmp (ie tmp != vide)
				tmp = tmp + ",\n" #on va à la ligne
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False) #on rajoute le contenu de la colonne MSG_IHEARD (écrit au bon format grâce à la méthode c_export_type)
		if(len(self.data[Message.MSG_THEY]) > 0): #s'il y a des messages dans la colonne MSG_THEY
			if tmp != "": #si on a déjà écrit le contenu d'une colonne dans tmp (ie tmp != vide)
				tmp = tmp + ",\n" #on va à la ligne
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False) #on rajoute le contenu de la colonne MSG_THEY (écrit au bon format grâce à la méthode c_export_type)
		tmp = "[" + tmp + "\n]" #on encadre tmp par des crochets pour un meilleur affichage 
		return tmp

	#a method to convert the object to string data
	def __str__(self): #pour bien mettre en forme l'affichage
		tmp="<catalog>\n" #on écrit la balise "catalog" suivi d'un retour à la ligne
		tmp=tmp+"\t<isaid>\n" #on rajoute la balise fille "isaid" suivi d'un retour à la ligne
		for msg in self.data[Message.MSG_ISAID]: #on parcourt la colonne des messages MSG_ISAID
			tmp = tmp+str(msg)+"\n" #on rajoute chaque élément de la colonne et on va à la ligne
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n" #on ferme la balise fille "\isaid", on va à la ligne, on ouvre la balise fille "iheard" suivi d'un retour à la ligne
		for msg in self.data[Message.MSG_IHEARD]: #on parcourt la colonne des messages MSG_IHEARD
			tmp = tmp+str(msg)+"\n" #on rajoute chaque élément de la colonne et on va à la ligne
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n" #on ferme la balise fille "\iheard", on va à la ligne, on ouvre la balise fille "theysaid" suivi d'un retour à la ligne
		for msg in self.data[Message.MSG_THEY]: #on parcourt la colonne des messages MSG_THEY
			tmp = tmp+str(msg)+"\n" #on rajoute chaque élément de la colonne et on va à la ligne
		tmp=tmp+"\n\t</theysaid>\n</catalog>" #on ferme la balise fille "\theysaid", on va à la ligne et on ferme la balise "catalogue"
		return tmp

	#Save object content to file
	def save(self):
		file = open(self.filepath,"w") #ouvrir le fichier en mode "write"
		file.write(self.c_export())
		file.close() #fermer le fichier
		return True #informer l'utilisateur que la sauvegarde s'est bien passée

	#add a Message object to the catalog
	def add_message(self, m, save=True):
		res = True #par défaut, on a ajouté le message
		if(self.check_msg(m.content, m.type)): #si le message est déjà dans le catalogue
			print(f"{m.content} is already there") #dire à l'utilisateur que le message est déjà dans le catalogue
			res = False #on n'a pas ajouté le message
		else: #si le message n'est pas encore dans le catalogue
			self.data[m.type].append(m) #on l'ajoute dans la colonne correspondante
			if save: #si on ne donne pas de paramètre
				res = self.save() #on sauvegarde le fichier par défaut
		return res

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False):
		if(msg_type is False): #si on ne donne pas de paramètre on purge toutes les colonnes
			self.purge(max_age, Message.MSG_ISAID) #méthode récursive
			self.purge(max_age, Message.MSG_IHEARD)
			self.purge(max_age, Message.MSG_THEY)
		else:
			removable = [] #on crée un tableau de ce que l'on va supprimer
			for i in range(len(self.data[msg_type])): #on parcourt une colonne du catalogue
				if(self.data[msg_type][i].age(True)>max_age): #si l'age du message > max_age (par défaut 14 jours)
					removable.append(i) #on rajoute l'élément courant au tableau removable
			while len(removable) > 0: #tant que le tableau removable n'est pas vide
					del self.data[msg_type][removable.pop()] #on supprime l'élément courant 
			self.save() #on sauvegarde le fichier
		return True #indique que la purge s'est bien passée

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):
		for msg in self.data[msg_type]: #on parcourt la colonne MSG_THEY (colonne déterminée par le paramètre msg_type)
			if msg_str == msg.content: #si le paramètre (élément recherché) correspond à l'élément courant du catalogue
				return True #le message est bien présent dans la colonne du catalogue
		return False #le message est absent de la colonne du catalogue

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4):
		self.purge() #on supprime tous les messages trop anciens (qui n'ont donc plus d'influence sur le diagnostic COVID)
		n = 0 #on initialise le compteur
		for msg in self.data[Message.MSG_IHEARD]: #on parcourt la colonne des messages MSG_IHEARD
			if self.check_msg(msg.content): #si le message est dans le catalogue
				n = n + 1 #on incrémente le compteur
		print(f"{n} covid messages heard") #on affiche le nombre de messages MSG_HEARD
		return max_heard < n #renvoie True si on a entendu plus de max_heard messages (par défaut 4) et False sinon

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json")
	catalog.add_message(Message())
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD))
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}"))
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2)
	print(catalog)
