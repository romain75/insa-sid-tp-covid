#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):
		if msg == "": #type par défault (si pas de premier paramètre rentré dans la création du message)
			self.content = Message.generate() #créer une chaine de caractère aléatoire
			self.type = Message.MSG_ISAID #type par défault = ISAID
			self.date = time.time() #enregistrer la date courante à l'instant où le message est généré
		elif msg_type is False : #si on ne donne pas de paramètre type dans la création du message
			self.m_import(msg) #on importe le message grâce à la méthode m_import()
		else:
			self.content = msg #si on a déjà mis une chaine de caractère dans les paramètres du message, pas besoin d'en générer une aléatoire
			self.type = msg_type #si on a déjà mis un type dans les paramètres du message, on le récupère
			if msg_date is False: #si on ne donne pas de paramètre date dans la création du message
				self.date = time.time() #on donne la date courante
			else: #si on a déjà mis une date dans les paramètres du message
				self.date = msg_date #on récupère la date définie lors de la création

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date) #afficher la date dans un format lisible

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False):
		age = time.time() - self.date #age = date courante - date de création du message (au format float en secondes)
		if days: #si on ne donne pas de paramètre days
			age = int(age/(3600*24)) #on convertit l'age en jours (au format int)
		if as_string: #si on donne un paramètre as_string
			d = int(age/(3600*24)) #calcule le nombre de jours
			r = age%(3600*24) #donne le reste une fois qu'on a compté les jours
			h = int(r/3600) #calcule le nombre d'heures
			r = r%3600 #donne le reste une fois qu'on a compté les jours et les heures
			m = int(r/60) #calcule le nombre de minutes
			s = r%60 #calcule le nombre de secondes
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s") #affichage l'age proprement avec des chaines de caractères en concaténant ce qu'on a calculé au-dessus
		return age

	#testers of message type
	def is_i_said(self):
		return self.type == Message.MSG_ISAID #déterminer si le message est de type ISAID
	def is_i_heard(self):
		return self.type == Message.MSG_IHEARD #déterminer si le message est de type IHEARD
	def is_they_said(self):
		return self.type == Message.MSG_THEY #déterminer si le message est de type THEY

	#setters
	def set_type(self, new_type):
		self.type = new_type #changer le type d'un message

	#a class method that generates a random message
	@classmethod
	def generate(cls):
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest() #générer une chaine de caractère aléatoire au format temps courant suivi de 8 caractères

	#a method to convert the object to string data
	def __str__(self):
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>" #affiche un message comme au format xml avec les balises (tabulations et retours à la ligne pris en comptes)

	#export/import
	def m_export(self):
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}" #envoyer un message avec un format standardisé

	def m_import(self, msg):
		if(type(msg) == type(str())): #si le msg est de type string
			json_object = json.loads(msg) #on converti le string en object json avec la fonction json.loads()
		elif(type(msg) == type(dict())): #si le msg est de type dict
			json_object = msg #pas besoin de convertir le message car dict en python est l'équivalent d'un object en json (cf cours p.57)
		else:
			raise ValueError #avertir l'utilisateur que le msg n'est pas au bon format
		self.content = json_object["content"] #récupérer le contenu du message
		self.type = json_object["type"] #récupérer le type du message
		self.date = json_object["date"] #récupérer la date du message

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() #créer un nouveau message qui s'appelle myMessage
	time.sleep(1) #attendre 1 seconde
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY) #créer un second message qui s'appelle mySecondMessage
	copyOfM = Message(myMessage.m_export())
	print(myMessage)
	print(mySecondMessage)
	print(copyOfM)
	time.sleep(0.5)
	print(copyOfM.age(True,True))
	time.sleep(0.5)
	print(copyOfM.age(False,True))
