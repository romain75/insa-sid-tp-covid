# #!/usr/bin/env python3
# from message import Message
# from message_catalog import MessageCatalog
# import requests

# class Client:
# 	#constructor, takes a message catalog
# 	def __init__(self, catalog, debug = False):
#         r.text=catalog #je reçois la réponse du serveur
# 		pass ## TODO:


# 	#send an I said message to a host
# 	def say_something(self, host):
#         myMessage = Message(Message.generate(), True) #je créé un message I said
#         r = requests.post(host, myMessage) #je l'envoie au serveur
# 		pass ## TODO:

# 	#add to catalog all the covid patient messages from host server
# 	def get_covid(self, host):
#         r = requests.get(host) #je reçois la liste des messages THEY SAID envoyée par le serveur
#         MessageCatalog.add_message(r.text) #j'ajoute le contenu des messages dans ma liste de messages
# 		pass ## TODO:


# 	#send to a server the list of “I said” messages
# 	def send_history(self, host): #pas sur du tout....
#         for i in MessageCatalog(): #je parcours ma liste de messages
#             if i.msg_type=True : #si c'est un message I said
#                 listIsaid[i]=i # j'ajoute ce message dans une liste de messages I said
#         r = requests.post(host, listIsaid)
# 		pass ## TODO:

# ## TODO: créer une sous classe de client pour tester le programme en lui donnant une liste de particuliers et un serveur hopital : tous les "say_something" seront pour tous les particuliers et get_covid et send_history pour l'hopital. Idéalement cette classe enverra des messages toutes les 5 minutes, purgera sa liste de messages à intervalles réguliers et après avoir fait get_covid diagnostiquera si l'utilisateur est cas contact ou non.

# if __name__ == "__main__":
# 	c = Client(MessageCatalog())
# 	c.say_something("http://localhost:5000")
# 	c.get_covid("http://localhost:5000")
# 	c.send_history("http://localhost:5000")


from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):
		self.catalog = MessageCatalog(catalog_path) #self.catalog prend la valeur de l'adresse du catalogue
		if debug: #si on donne le paramètre debug=True
			print(self.catalog) #on affiche le chemin du catalogue
		self.r = None
		self.debug = debug
		self.protocol = defaultProtocol

	@classmethod
	def withProtocol(cls, host):
		res = host.find("://") > 1 #on recherche la chaine de caractère "://" dans host : si elle est présente, alors res=True (indique qu'on utilise un protocole non standard et False sinon (indique qu'on utilise le protocole par défaut)
		return res

	def completeUrl(self, host, route = ""):
		if not Client.withProtocol(host): #si on ne veut pas utiliser un protocole spécial
			host = self.protocol + host #on concatène la valeur de defaultProtocol (http://) avec l'url souhaitée
		if route != "": #si on indique une route spécifique dans les paramètres
			route = "/"+route #on prend en compte cette route
		return host+route #on retourne l'adresse complète au format désiré

	#send an I said message to a host
	def say_something(self, host):
		m = Message() #on crée un nouveau message qui s'appelle m de type ISAID (pas de paramètre renseigné)
		self.catalog.add_message(m) #on ajoute ce message au catalogue
		route = self.completeUrl(host, m.content) #on définie l'adresse où on veut aller (ici un message particulier identifié par m.content)
		self.r = requests.post(route) #on envoie la requête post à l'adresse définie
		if self.debug: #si on est en mode debug (spécifié dans le constructeur de Client)
			print("POST  "+route + "→" + str(self.r.status_code)) #on affiche la réponse du serveur pour vérifier que la requête a bien été traitée
			print(self.r.text) #on affiche le contenu de la requête au format text (pour savoir quelle requête a été envoyée)
		return self.r.status_code == 201 #on vérifie qu'on a bien créé et envoyé le message (201 Created) : on retourne True si oui et False sinon

	#add to catalog all the covid from host server
	def get_covid(self, host):
		route = self.completeUrl(host,'/they-said') #on définit l'adresse où on veut aller (ici THEY_SAID)
		self.r = requests.get(route) #on envoie la requête get à l'adresse définie
		res = self.r.status_code == 200 #on vérifie que la requête a bien été traitée (200 OK) : res=True si oui et False sinon
		if res:#si la requête a bien été traitée
			res = self.catalog.c_import(self.r.json()) #on importe dans le catalogue la réponse de la requête (r.json), ie les messages COVID+ venant du serveur/hopital
		if self.debug: #si on est en mode debug
			print("GET  "+ route + "→" + str(self.r.status_code)) #on affiche la réponse du serveur pour vérifier que la requête a bien été traitée
			if res != False: #si la requête a bien été traitée
				print(str(self.r.json())) #on affiche le contenu de la requête au format json (pour savoir quelle requête a été envoyée)
		return res #on vérifie qu'on a bien importé le catalogue venant du serveur (200 OK)

	#send to server list of I said messages
	def send_history(self, host):
		route = self.completeUrl(host,'/they-said') #on définie l'adresse où on veut aller (ici THEY_SAID)
		self.catalog.purge(Message.MSG_ISAID) #on purge les messages ISAID trop anciens (age>14)
		data = self.catalog.c_export_type(Message.MSG_ISAID) #on exporte du catalogue, dans un format standardisé, les messages ISAID restants après la purge
		self.r = requests.post(route, json=data) #on envoie la requête post avec son contenu (messages ISAID) à l'adresse définie
		if self.debug: #si on est en mode debug
			print("POST  "+ route + "→" + str(self.r.status_code)) #on affiche la réponse du serveur pour vérifier que la requête a bien été traitée
			print(str(data)) #on affiche ce que l'on a envoyé (liste des messages ISAID)
			print(str(self.r.text)) #on affiche le contenu de la requête au format text
		return self.r.status_code == 201 #on vérifie qu'on a bien créé et envoyé la liste des messages ISAID (201 Created)


if __name__ == "__main__":
	c = Client("client.json", True)
	c.say_something("localhost:5000")
	c.get_covid("localhost:5000")
	c.send_history("localhost:5000")
